from run_backend import app
from flask import jsonify, request
from models import Password
from database import db_session
from sqlalchemy.exc import DBAPIError


@app.route('/api/v1.0/users/<int:user_id>/passwords/<password_alias>', methods=['GET'])
def get_users_password_v1_0(user_id, password_alias):
    token = request.args.get('token', '')

    try:
        row = Password.query\
            .filter(Password.user_id == user_id, Password.password_alias == password_alias)\
            .order_by(Password.password_version.desc())\
            .first()

        password_version = row.password_version
        encrypted_password = row.encrypted_password

        s_password_version = str(password_version).zfill(2)

        resp = s_password_version + encrypted_password
        resp.status_code = 200

    except DBAPIError as dbApiError:
        resp = jsonify(
            error_desc=dbApiError.detail
        )
        resp.status_code = 500

    return resp


@app.route('/api/v1.0/users/<int:user_id>/passwords/<password_alias>', methods=['POST'])
def set_users_password_v1_0(user_id, password_alias):
    token = request.args.get('token', '')
    print(request)

    print(request.content)
    encrypted_password = request.content

    try:
        old_password = Password.query\
            .filter(Password.user_id == user_id, Password.password_alias == password_alias)\
            .order_by(Password.password_version.desc())\
            .first()
        password = Password(password_alias, encrypted_password, user_id)
        if old_password is not None:
            if old_password.encrypted_password != password.encrypted_password:
                password.password_version = old_password.password_version + 1
            else:
                resp = jsonify(password_version=old_password.password_version)
                resp.status_code = 200
                return resp

        db_session.add(password)
        db_session.commit()
        resp = jsonify(password_version=password.password_version)
        resp.status_code = 200

    except DBAPIError as dbApiError:
        resp = jsonify(
            error_desc=dbApiError.detail
        )
        resp.status_code = 500

    return resp
