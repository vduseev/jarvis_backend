from run_backend import app
from flask import jsonify, request
from models import User
from sqlalchemy import exc


@app.route('/api/v1.0/users/<int:user_id>', methods=['GET'])
def get_user_by_id(user_id):
    token = request.args.get('token', '')
    try:
        row = User.query.filter(User.user_id == int(user_id)).first()
        user = {
            'user_id': row.user_id,
            'login': row.login,
            'givenname': row.givenname,
            'surname': row.surname
        }
        resp = jsonify(user)
        resp.status_code = 200
    except exc.DatabaseError:
        resp = jsonify(error='database error')
        resp.status_code = 500
    return resp


@app.route('/api/v1.0/users/', methods=['GET'])
def get_users():
    token = request.args.get('token', None)
    login = request.args.get('login', None)
    if login is None:
        rows = User.query.all()
        users = []
        for row in rows:
            user = {
                'user_id': row.user_id,
                'login': row.login,
                'givenname': row.givenname,
                'surname': row.surname
            }
            users.append(user)

        body = {
            'items': users,
            'count': len(users)
        }

        resp = jsonify(body)
        resp.status_code = 200
        return resp
    else:
        row = User.query.filter(User.login == login).first()
        user = {
            'user_id': row.user_id,
            'login': row.login,
            'givenname': row.givenname,
            'surname': row.surname
        }
        resp = jsonify(user)
        resp.status_code = 200
        return resp
