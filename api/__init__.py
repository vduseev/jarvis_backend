from flask import Flask

from database import db_session

app = Flask(__name__)
app.debug = True


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


import api.index
import api.users
import api.passwords
