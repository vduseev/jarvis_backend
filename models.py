from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.orm import mapper
from database import metadata, db_session
from json import dumps


class User(object):
    query = db_session.query_property()

    def __init__(self, login=None, email=None, givenname=None, surname=None):
        self.login = login
        self.email = email
        self.givenname = givenname
        self.surname = surname
        self.password = None

    def __repr__(self):
        body = {
            'user_id': int(self.user_id),
            'login': str(self.login),
            'email': str(self.email),
            'givenname': str(self.givenname),
            'surname': str(self.surname)
        }
        return dumps(body, indent=2)

users = Table(
    'users',
    metadata,
    Column('user_id', Integer, primary_key=True),
    Column('login', String(30), unique=True),
    Column('email', String(60), unique=True),
    Column('password', String(256)),
    Column('givenname', String(256)),
    Column('surname', String(256)),
    schema='jarvis'
)
mapper(User, users)


class Password(object):
    query = db_session.query_property()

    def __init__(self, password_alias=None, encrypted_password=None, user_id=None):
        self.password_alias = password_alias
        self.password_version = 1
        self.encrypted_password = encrypted_password
        self.user_id = user_id

    def __repr__(self):
        body = {
            'password_id': int(self.password_id),
            'password_alias': str(self.password_alias),
            'password_version': int(self.password_version),
            'user_id': int(self.user_id)
        }
        return dumps(body, indent=2)

passwords = Table(
    'passwords',
    metadata,
    Column('password_id', Integer, primary_key=True),
    Column('password_alias', String(256)),
    Column('password_version', Integer),
    Column('encrypted_password', String),
    Column('user_id', Integer),
    schema='jarvis'
)
mapper(Password, passwords)
